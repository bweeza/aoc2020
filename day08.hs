
import Data.Set (Set, empty, insert, member)

data Machine = Machine
  { acc :: Int
  , ip :: Int
  , store :: [String]
  , seen :: Set Int
  }
  deriving (Show, Eq)

defaultMachine :: Machine
defaultMachine =
  Machine { acc = 0, ip = 0, store = [], seen = empty }

nextInstruction :: Machine -> (String, Int)
nextInstruction m =
  (instr, n)
  where
    (instr, ntxt) = splitAt 3 . (!! ip m). store $ m
    n :: Int
    n = read . dropWhile (\ch -> ch == '+' || ch == ' ') $ ntxt

step :: Machine -> Machine
step m =
  m { acc = acc', ip = ip', seen = seen' }
  where
    (instr, n) = nextInstruction m

    acc' = case instr of
      "acc" -> acc m + n
      _ -> acc m

    ip' = case instr of
      "jmp" -> ip m + n
      _ -> ip m + 1

    seen' = insert (ip m) (seen m)

naughty :: Machine -> Bool
naughty m = member (ip m) (seen m) || ip m >= length (store m)

stoppedWell :: Machine -> Bool
stoppedWell m = ip m == length (store m)

runMachine :: Machine -> Machine
runMachine =
  head . dropWhile (not .naughty) . iterate step

tweakedPrograms :: Machine -> [Machine]
tweakedPrograms m =
  concat [tweakAt n m | n <- [0..length (store m) - 1]]
  where
    tweakAt :: Int -> Machine -> [Machine]
    tweakAt ip' m =
      case nextInstruction m{ip=ip'} of
        ("jmp", n) -> [m, m{ store = repl ip' ("nop " ++ show n) (store m)}]
        ("nop", n) -> [m, m{ store = repl ip' ("jmp " ++ show n) (store m)}]
        _ -> [m]
    repl :: Int -> a -> [a] -> [a]
    repl _ _ [] = []
    repl n x' (x:xs)
      | n == 0 = x':xs
      | otherwise = x:repl (n-1) x' xs

main :: IO ()
main = do
  part1
  part2

part2 :: IO ()
part2 = do
  testdata <- readFile "day08.testdata"
  let m0 = defaultMachine { store = lines testdata } in
    print $ result m0 . dropWhile (not . stoppedWell) . map runMachine $ tweakedPrograms m0

  realdata <- readFile "day08.data"
  let m = defaultMachine { store = lines realdata } in
    print $ result m . dropWhile (not . stoppedWell) . map runMachine $ tweakedPrograms m
  where
    result _m_orig [] = Nothing
    result _m_orig (x:_) = Just $ acc x

part1 :: IO ()
part1 = do
  testdata <- readFile "day08.testdata"
  print $ acc . head . dropWhile (not . naughty) . iterate step $ defaultMachine { store = lines testdata }
  realdata <- readFile "day08.data"
  print $ acc . head . dropWhile (not . naughty) . iterate step $ defaultMachine { store = lines realdata }
