

main :: IO ()
main = do
  print "test"
  print $ length $ validPasswords testData
  print $ length $ realValidPasswords testData
  print "real"
  realData <- readFile "day02.data"
  print $ length $ validPasswords realData
  print $ length $ realValidPasswords realData
  where
    realValidPasswords =
      filter id . map (uncurry realValidate . parse) . lines
    validPasswords =
      filter id . map (uncurry validate . parse) . lines


testData :: String
testData =  init $ unlines
  ["1-3 a: abcde",
   "1-3 b: cdefg",
   "2-9 c: ccccccccc"]

data Rule = Rule Int Int Char deriving (Read, Show)

parse :: String -> (Rule, String)
parse input =
  (Rule minoccur maxoccur ch, password)
  where
    minoccur = read $ takeWhile (/= '-') input
    maxoccur = read $ takeWhile (/= ' ') $ tail $ dropWhile (/= '-') input
    ch = head $ tail $ dropWhile (/= ' ') input
    password = tail $ tail $ dropWhile (/= ':') input

validate :: Rule -> String -> Bool
validate (Rule minoccur maxoccur ch) password =
  minoccur <= occur && occur <= maxoccur
  where
    occur = length [c | c <- password, c == ch]

realValidate :: Rule -> String -> Bool
realValidate (Rule pos1 pos2 ch) password =
  (charAt pos1 == ch) `xor` (charAt pos2 == ch)
  where
    charAt :: Int -> Char
    charAt n = password !! (n-1)

    xor True a = not a
    xor False a = a
