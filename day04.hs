
import Data.Text (pack, unpack, splitOn)
import Data.Maybe
import Text.ParserCombinators.Parsec

type Passport = [(String, String)]

required :: [String]
required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]  -- We're skipping "cid"

main :: IO ()
main = do
  print "Test"
  testData <- readFile "day04.testdata"
  let passports = [ v | Right v <- map (parse passport "") (chunk testData)] in
    printResults passports

  print "\nReal"
  realData <- readFile "day04.data"
  let passports = [ v | Right v <- map (parse passport "") (chunk realData)] in
    printResults passports

  where
    chunk d = map unpack $ splitOn (pack "\n\n") (pack d)
    printResults passports = do
      print "Sloppy Valid"
      print $ length $ filter hasAllFields passports
      print "Strict Valid"
      print $ length $ filter validate passports

hasAllFields :: [(String, a)] -> Bool
hasAllFields pass = all isJust [lookup k pass | k <- required]

validate :: Passport -> Bool
validate pass =
  hasAllFields pass && all (uncurry validEntry) pass

validEntry :: String -> String -> Bool
validEntry "byr" val =
  case fullParse integer "" val of
    Left _ -> False
    Right year -> 1920 <= year && year <= 2002 && length val == 4

validEntry "iyr" val =
  case fullParse integer "" val of
    Left _ -> False
    Right year -> 2010 <= year && year <= 2020 && length val == 4

validEntry "eyr" val =
  case fullParse integer "" val of
    Left _ -> False
    Right year -> 2020 <= year && year <= 2030 && length val == 4

validEntry "hgt" val =
  case fullParse height "" val of
    Left _ -> False
    Right (h, "cm") -> 150 <= h && h <= 193
    Right (h, "in") -> 59 <= h && h <= 76
    Right (_, _) -> False

validEntry "hcl" (hash:hexval) =
  case fullParse hex "" hexval of
    Left _ -> False
    Right _ -> length hexval == 6 && hash == '#'

validEntry "ecl" val =
  val `elem` ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

validEntry "pid" val =
  case fullParse integer "" val of
    Left _ -> False
    Right _ -> length val == 9

validEntry _ _ = True

passport :: GenParser Char st Passport
passport = sepBy entry spaces

key :: GenParser Char st String
key = many (noneOf ":")

value :: GenParser Char st String
value = many (noneOf " \t\n")

entry :: GenParser Char st (String, String)
entry = do
  k <- key
  _ <- char ':'
  v <- value
  return (k, v)

-- Fields
integer :: GenParser Char st Int
integer = do
  digits <- many1 digit
  return (read digits)

height :: GenParser Char st (Int, String)
height = do
  h <- integer
  unit <- choice [string "cm", string "in"]
  return (h, unit)

hex :: GenParser Char st Int
hex = do
  _ <- many1 hexDigit
  return 0 -- XXX This is wrong but I don't think we care

fullParse :: GenParser Char () a -> SourceName -> String -> Either ParseError a
fullParse p = parse (blah p)

blah :: GenParser Char st a -> GenParser Char st a
blah p = p <* eof
