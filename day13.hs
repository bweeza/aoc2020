
import Data.List (sortOn)
import Data.List.Split (splitOn)

type Wait = Int
type Bus = Int

test :: [String]
test = lines "939\n7,13,x,x,59,x,31,19\n"

real :: [String]
real = lines "1003240\n19,x,x,x,x,x,x,x,x,41,x,x,x,37,x,x,x,x,x,787,x,x,x,x,x,x,x,x,x,x,x,x,13,x,x,x,x,x,x,x,x,x,23,x,x,x,x,x,29,x,571,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,17\n"


main :: IO ()
main = do
  partOne test
  partOne real
  print . partTwo . intervals $ test
  print . partTwo . intervals $ real

partOne :: [String] -> IO ()
partOne dat =
  let earliest :: Int
      earliest = read . head $ dat
      buses :: [Int]
      buses = map read . filter (/= "x") . splitOn "," . head . tail $ dat
  in
    print . uncurry (*) . head . sortOn snd  $ [(bus, bus - (earliest `mod` bus) ) | bus <- buses]

partTwo :: [(Wait, Bus)] -> Int
partTwo sched = partTwo' 1 start sched
  where
    start = snd . head $ sched

partTwo' :: Int -> Int -> [(Wait, Bus)] -> Int
partTwo' _ start [] = start
partTwo' step start ((wait, bus):rest) =
  partTwo' (step * bus) start' rest
  where
    start' = whenWeWait wait bus start step

whenWeWait :: Wait -> Bus -> Int -> Int -> Int
whenWeWait wait bus start step =
    head . dropWhile (\t-> (t `mod` bus) /= ((bus-wait)`mod`bus)) $ [start + s | s <- [0, step..]]

intervals :: [String] -> [(Wait, Bus)]
intervals = map (\(w,b) -> (w, read b)) . filter ((/="x").snd) . zip [0..] . splitOn "," .  (!!1)
