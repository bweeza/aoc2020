
import Text.Printf (printf)

type Heading = Int
type Waypoint = (Int, Int)
type Location = (Int, Int)
type State = (Heading, Location)
type State2 = (Waypoint, Location)
type Instr = String

start :: State
start = (90, (0, 0))

start2 :: State2
start2 = ((1,10), (0, 0))

main :: IO ()
main = do
  t <- lines <$> readFile "day12.testdata"
  d <- lines <$> readFile "day12.data"

  print "Part One"
  printf "Test: %i\n" $ manhattan . snd . foldl move start $ t
  printf "Real: %i\n" $ manhattan . snd . foldl move start $ d

  print "---------------------"
  print "Part Two"
  printf "Test: %i\n" $ manhattan . snd . foldl move2 start2 $ t
  printf "Real: %i\n" $ manhattan . snd . foldl move2 start2 $ d

move2 :: State2 -> Instr -> State2
move2 ((x, y), p) ('N':d) = ((x + read d, y), p)
move2 ((x, y), p) ('S':d) = ((x - read d, y), p)
move2 ((x, y), p) ('E':d) = ((x, y + read d), p)
move2 ((x, y), p) ('W':d) = ((x, y - read d), p)
move2 (w@(wx, wy), (x, y)) ('F':d) = (w, (x + read d * wx, y + read d * wy))
move2 (w, p) ('L':d) = (rotate w (negate $ read d), p)
move2 (w, p) ('R':d) = (rotate w (read d), p)
move2 _ _ = error "wtf?"

rotate :: Waypoint -> Int -> Waypoint
rotate p@(x, y) r
  | r < 0 || r >= 360 = rotate p ((r+360) `mod` 360)
  | otherwise =
    case r of
      0 -> (x, y)
      90 -> (negate y, x)
      180 -> (-x, -y)
      270 -> (y, negate x)
      _ -> error $ "bad rotation: " ++ show r

move :: State -> Instr -> State
move (h, (x, y)) ('N':dtxt) = (h, (x + read dtxt, y))
move (h, (x, y)) ('S':dtxt) = (h, (x - read dtxt, y))
move (h, (x, y)) ('E':dtxt) = (h, (x, y + read dtxt))
move (h, (x, y)) ('W':dtxt) = (h, (x, y - read dtxt))
move (h, (x, y)) ('L':dtxt) = ((h - read dtxt + 360) `mod` 360, (x,y))
move (h, (x, y)) ('R':dtxt) = ((h + read dtxt) `mod` 360, (x,y))
move s@(h, _) ('F':dtxt) =
  case h of
    0 -> move s ('N':dtxt)
    90 -> move s ('E':dtxt)
    180 -> move s ('S':dtxt)
    270 -> move s ('W':dtxt)
    _ -> error ("bad heading: " ++ show h)
move _ _ = error "wtf?"

manhattan :: Location -> Int
manhattan (x, y) = abs x + abs y
