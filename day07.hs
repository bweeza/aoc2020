
import Control.Applicative (optional, many, (<|>))
import Data.Functor (($>), void)
import Data.Set (Set, fromList)
import Data.Either (rights)
import Text.Megaparsec (Parsec, try, runParser)
import Text.Megaparsec.Char (space, space1, char, string, alphaNumChar)
import Text.Megaparsec.Char.Lexer (decimal, decimal)
import Control.Applicative.Combinators (sepBy)
import Data.Void (Void)

type Description = String
type Contents = [(Int, Description)]
type Bags = [Bag]
data Bag = Bag Description Contents deriving (Read, Show)

findBag :: Bags -> Description -> Bag
findBag bags c = head [bag | bag@(Bag cc _) <- bags, c == cc]


main :: IO ()
main = do
  -- part1
  part2

-- where can we hide shiny a gold bag?
part1 :: IO ()
part1 = do
  testbags <- parseBags <$> readFile "day07.testdata"
  let holders = holds "shiny gold" testbags in
    print . length $ holders

  bags <- parseBags <$> readFile "day07.data"
  let holders = holds "shiny gold" bags in
    print . length $ holders

holds :: Description -> Bags -> Set Description
holds desc allBags =
  fromList [d | bag@(Bag d _) <- allBags, isChild allBags bag desc && d /= desc]

isChild :: Bags -> Bag -> Description -> Bool
isChild bags (Bag pColor contents) color =
  pColor == color || or [isChild bags (findBag bags c) color | (_,c) <- contents]

-- count the bags in a bag
part2 :: IO ()
part2 = do
  testBags <- parseBags <$> readFile "day07.testdata2"
  print $ countEm testBags (findBag testBags "shiny gold")

  bags <- parseBags <$> readFile "day07.data"
  print $ countEm bags (findBag bags "shiny gold")

countEm :: Bags -> Bag -> Int
countEm _ (Bag _ []) = 0
countEm bags (Bag _ contents@(_:_)) =
  sum [ n * (countEm bags (findBag bags color) + 1) | (n, color) <- contents ]

-- Parsing

type Parser = Parsec Void String

parseBags :: String -> Bags
parseBags inp =
  rights $ map (runParser pBag "") (lines inp)

pBagsContain :: Parser ()
pBagsContain = string "bags contain" $> ()

pBagLit :: Parser ()
pBagLit = void (string "bag") <* optional (char 's')

pDescription :: Parser String
pDescription = (\x y -> x ++ " " ++ y) <$> many alphaNumChar <* space1 <*> many alphaNumChar

pNum :: Parser Int
pNum = decimal

pNoContents :: Parser [a]
pNoContents = [] <$ string "no other bags"

pContent :: Parser (Int, Description)
pContent = (,) <$> decimal <* space1 <*> pDescription <* space <* pBagLit

pContents :: Parser [(Int, Description)]
pContents = try pNoContents <|>
  sepBy pContent (char ',' <* space)

pBag :: Parser Bag
pBag = do
  desc <- pDescription
  space
  pBagsContain
  space
  contents <- pContents
  space
  void (char '.')
  return $ Bag desc contents

pBag' :: Parser Bag
pBag' = Bag <$> pDescription <* space <* pBagsContain <* space <*> pContents <* space <* char '.'
