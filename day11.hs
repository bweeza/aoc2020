
import Text.Printf (printf)

-- # Person
-- . Floor
-- L Empty Seat

type Room = [String]

main :: IO ()
main = do
  testRoom0 <- lines <$> readFile "day11.testdata"
  room0 <- lines <$> readFile "day11.data"

  printf "Part One\n"
  printf "Test: %i\n" (countPeople . fixPoint $ iterate step testRoom0)
  showRoom . fixPoint $ iterate step testRoom0
  printf "Real: %i\n" (countPeople . fixPoint $ iterate step room0)

  printf "\n\n"
  printf "Part Two\n"
  printf "\n"
  printf "Test: %i\n" (countPeople . fixPoint $ iterate step2 testRoom0)
  printf "Real: %i\n" (countPeople . fixPoint $ iterate step2 room0)
  -- showRoom . fixPoint $ iterate step2 testRoom0

answer :: [Room] -> Int
answer rooms =
  countPeople . fst . head . dropWhile (uncurry (/=)) $ zip rooms (tail rooms)

fixPoint :: [Room] -> Room
fixPoint rooms =
  fst . head . dropWhile (uncurry (/=)) $ zip rooms (tail rooms)

-- ------------------------------------------------------------------
maxes :: [[a]] -> (Int, Int)
maxes room = (length room -1, length (head room) -1)

sightVectors :: (Int, Int) -> (Int, Int) -> [[(Int, Int)]]
sightVectors (maxX, maxY) (x, y) =
  map (filter inBounds)
  [
    [(x', y) | x' <- [x+1..maxX]],
    [(x', y) | x' <- [x-1, x-2..0]],
    [(x, y') | y' <- [y+1..maxY]],
    [(x, y') | y' <- [y-1, y-2..0]],

    [(x+i, y+i) | i <- [0..maxAll]],
    [(x+i, y-i) | i <- [0..maxAll]],
    [(x-i, y+i) | i <- [0..maxAll]],
    [(x-i, y-i) | i <- [0..maxAll]]
  ]
  where
    maxAll = max maxX maxY
    inBounds (a, b) =
      a >= 0 && b >= 0 && a <= maxX && b <= maxY && (a,b) /= (x,y)

step2 :: Room -> Room
step2 room =
  [[ nextTile i j | j <- [0..maxJ] ] | i <- [0..maxI]]
  where
    (maxI, maxJ) = maxes room

    nextTile :: Int -> Int -> Char
    nextTile i j =
      case room !! i !! j of
        '.' -> '.'
        'L' -> if numVisible room i j == 0 then '#' else 'L'
        '#' -> if numVisible room i j >= 5 then 'L' else '#'
        _ -> error "corrupted map"

numVisible :: Room -> Int -> Int -> Int
numVisible room i j =
  length . filter (== '#') . concat $
    [ take 1 . dropWhile (== '.') $
      [ room!!x!!y | (x,y) <- vec]
    | vec <- sightVectors (maxI, maxJ) (i, j) ]
  where
    (maxI, maxJ) = maxes room

-- ------------------------------------------------------------------

step :: Room -> Room
step room =
  [[ nextTile i j | j <- [0..maxJ] ] | i <- [0..maxI]]
  where
    maxI = length room - 1
    maxJ = length (head room) - 1

    nextTile :: Int -> Int -> Char
    nextTile i j =
      case room !! i !! j of
        '.' -> '.'
        'L' -> if numNeighbors '#' i j == 0 then '#' else 'L'
        '#' -> if numNeighbors '#' i j >= 4 then 'L' else '#'
        _ -> error "corrupted map"

    numNeighbors :: Char -> Int -> Int -> Int
    numNeighbors ch i j =
      length . filter (== ch) $ [room !! x !! y | (x,y) <- neighbors i j]

    neighbors :: Int -> Int -> [(Int, Int)]
    neighbors i j =
      [(i', j') |
       i' <- [i-1..i+1], i'>=0, i'<=maxI,
       j' <- [j-1..j+1], j'>=0, j'<=maxJ, (i',j') /= (i,j)
      ]

countPeople :: Room -> Int
countPeople =
  length . filter (== '#') . concat

showRoom :: Room -> IO ()
showRoom [] = return ()
showRoom (x:xs) = do
  print x
  showRoom xs
