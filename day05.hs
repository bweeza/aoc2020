import Data.List (sort)

main :: IO ()
main = do
  print "Test"
  testData <- readFile "day05.testdata"
  print $ map seatId $ lines testData
  print $ maximum $ map seatId $ lines testData
  print "Real"
  seatIds <- map seatId . lines <$> readFile "day05.data"
  print $ maximum seatIds
  print $ findGaps (sort seatIds) []

seatId :: String -> Int
seatId code =
  8 * rowNum + seatNum
  where
    rowNum = decodePos 0 127 $ take 7 code
    seatNum = decodePos 0 7 $ drop 7 code

decodePos :: Int -> Int -> String -> Int
decodePos minx maxx code =
  case reduceRange code (minx, maxx) of
    (low, high) -> if low == high then low else undefined

reduceRange :: String -> (Int, Int) -> (Int, Int)
reduceRange [] (low, high) = (low, high)
reduceRange (code:codes) range =
  reduceRange codes $ reduceRange1 code range

reduceRange1 :: Char -> (Int, Int) -> (Int, Int)
reduceRange1 code (low, high) =
  if code `elem` "FL"
  then (low, high - change)
  else (low + change, high)
  where
    change = (1 + high - low) `div` 2


findGaps :: [Int] -> [Int] -> [Int]
findGaps [] sofar = sofar
findGaps [_] sofar = sofar
findGaps (seat:nextSeat:seats) sofar =
  if (seat+1) /= nextSeat
  then (seat+1):sofar
  else findGaps (nextSeat:seats) sofar
