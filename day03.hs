
data Map = Map Int Int [[Bool]] deriving (Read, Show)
data Point = Point Int Int deriving (Read, Show)

type Slope = (Int, Int)


main :: IO ()
main = do
  print "Test data - expect 7"
  testData <- readFile "day03.testdata"
  print $ numTrees (parseMap testData) (1,3)
  print "Real Data"
  realData <- readFile "day03.data"
  print $ numTrees (parseMap realData) (1,3)
  print "Part Two - Test Data. Expect 336"
  print $ product $ map (numTrees (parseMap testData)) slopes
  print "Real Data"
  print $ map (numTrees (parseMap realData)) slopes
  print $ product $ map (numTrees (parseMap realData)) slopes
  print "3944592960 is too high"
  where
    slopes = [(1,1), (1,3), (1,5), (1,7), (2,1)]

numTrees :: Map -> Slope -> Int
numTrees treeMap slope =
  length
  $ filter id
  $ map (treeAt treeMap)
  $ takeWhile (onMap treeMap)
  $ makeLine slope (Point 0 0)

treeAt :: Map -> Point -> Bool
treeAt (Map xmax ymax trees) (Point x y) =
  trees !! (x `rem` xmax) !! (y `rem` ymax)

nextPoint :: Slope -> Point -> Point
nextPoint (xlen, ylen) (Point x y) = Point (x+xlen) (y+ylen)

makeLine :: Slope -> Point -> [Point]
makeLine = iterate . nextPoint

onMap :: Map -> Point -> Bool
onMap (Map nrows _ _) (Point x _) = x < nrows

parseMap :: String -> Map
parseMap input =
  Map nrows ncols trees
  where
    rows = lines input
    nrows = length rows
    ncols = length $ head rows
    trees = [[isTree dot | dot <- row] | row <- rows]
    isTree '#' = True
    isTree '.' = False
    isTree _ = undefined
