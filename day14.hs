
import Data.Bits ((.&.), (.|.), xor, shift, complement)
import Data.Map (Map, empty, insert, toList)
import Data.Either (fromRight)

import Control.Applicative (many, (<|>))
import Data.Void (Void)
import Data.Functor (void)
import Text.Megaparsec (Parsec, runParser, eof)
import Text.Megaparsec.Char (string, alphaNumChar, newline)
import Text.Megaparsec.Char.Lexer (decimal, decimal)

type State = (Mask, Map Int Int)
type Mask = (Int, Int, [Int])

state0 :: State
state0 = (undefined, empty)

data Instruction =
  Bitmask Mask |
  MemInst (Int, Int)
  deriving Show

main :: IO ()
main = do
  td <- readFile "day14.testdata"
  d <- readFile "day14.data"
  print "Part One"
  print "Test Data"
  print . computeAnswer . foldl step state0 $ parsed td
  print "Real Data"
  print . computeAnswer . foldl step state0 $ parsed d
  print $ replicate 20 '-'
  print "Part Two"
  print "Test Data"
  td2 <- readFile "day14.testdata2"
  print . computeAnswer . foldl step2 (undefined, empty) $ parsed td2
  print "Real Data"
  print . computeAnswer . foldl step2 (undefined, empty) $ parsed d

computeAnswer :: State -> Int
computeAnswer (_mask, mem)= sum [val | (_, val) <- toList mem]

step :: State -> Instruction -> State
step (_, mem) (Bitmask b) = (b, mem)
step (mask, mem) (MemInst (loc, val)) =
  (mask, insert loc (applyMask mask val) mem)

applyMask :: Mask -> Int -> Int
applyMask (ones, zeros, _exes) i =
  (i .|. ones) .&. complement zeros

step2 :: State -> Instruction -> State
step2 (_, mem) (Bitmask b) = (b, mem)
step2 (mask, mem) (MemInst (loc, val)) =
  (mask, doInsert (applyMask2 mask loc) mem)
  where
    doInsert [] m = m
    doInsert (l:ls) m = insert l val (doInsert ls m)

applyMask2 :: Mask -> Int -> [Int]
applyMask2 (ones, _, exes) i =
  doExes [n] exes
  where
    n = i .|. ones

doExes :: [Int] -> [Int] -> [Int]
doExes locs [] = locs
doExes locs (b:bs) =
  doExes (concat [ [loc, loc `xor` (1 `shift` b)] | loc <- locs]) bs


------------------------------------------------------------------
-- Parsing Junk
------------------------------------------------------------------

type Parser = Parsec Void String

parsed :: String -> [Instruction]
parsed = fromRight undefined . runParser pProgram ""

pProgram :: Parser [Instruction]
pProgram =
  many ( (pBitmask <|> pMemInstr) <* newline ) <* eof

pBitmask :: Parser Instruction
pBitmask = do
  void $ string "mask = "
  bmstr <- many alphaNumChar
  return $ Bitmask $ mkBitmask bmstr
  where
    mkBitmask bmstr = (ones, zeros, exes)
      where
        ones = readBits 0 [if ch == '1' then '1' else '0' | ch <- bmstr]
        zeros = readBits 0 [if ch == '0' then '1' else '0' | ch <- bmstr]
        exes = [pos | (val, pos) <- zip (reverse bmstr) [0..], val == 'X']

readBits :: Int -> String -> Int
readBits acc [] = acc
readBits acc ('0':xs) = readBits (2*acc + 0) xs
readBits acc ('1':xs) = readBits (2*acc + 1)  xs
readBits _ (_:_) = undefined

pMemInstr :: Parser Instruction
pMemInstr = do
  void (string "mem[")
  a <- decimal
  void (string "] = ")
  b <- decimal
  return $ MemInst (a,b)
