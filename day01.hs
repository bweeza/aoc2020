
main :: IO ()
main = do
  print $ uncurry (*) $ head $ sumsTo 2020 testData
  realData <- readFile "day01.data"
  print $ uncurry (*) $ head $ sumsTo 2020 $ map read $ lines realData
  print "Part 2"
  print $ (\(a, b, c) -> a * b * c) $ head $ threeSumTo 2020 testData
  print $ (\(a, b, c) -> a * b * c) $ head $ threeSumTo 2020 $ map read $ lines realData

testData :: [Int]
testData = [1721, 979, 366, 299, 675, 1456]

sumsTo :: Int -> [Int] -> [(Int, Int)]
sumsTo n xs =
  [ (a, b) | (a, bs) <- splitsy xs, b <- bs, a + b == n ]
  where
    splitsy :: [Int] -> [(Int, [Int])]
    splitsy x = zip (init x) (iterate tail (tail x))


threeSumTo :: Int -> [Int] -> [(Int, Int, Int)]
threeSumTo n xs =
  [ (a, b, c) | (a, bs) <- splitsy xs, (b, cs) <- splitsy bs, c <- cs, a + b + c == n ]
  where
    splitsy :: [Int] -> [(Int, [Int])]
    splitsy x = zip (init x) (iterate tail (tail x))
