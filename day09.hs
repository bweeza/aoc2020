
main :: IO ()
main = do
  testdata <- map read . lines <$> readFile "day09.testdata"
  let
    bad = findBad 5 testdata
    contiguous = findContiguous bad testdata
    in do
      print bad
      print $ maximum contiguous + minimum contiguous

  realdata <- map read . lines <$> readFile "day09.data"
  let
    bad = findBad 25 realdata
    contiguous = findContiguous bad realdata
    in do
      print bad
      print $ maximum contiguous + minimum contiguous

findBad :: Int -> [Int] -> Int
findBad _ [] = -1
findBad prefixLen xs@(_:rest)
  | not $ satisfies prefixLen xs = xs !! prefixLen
  | otherwise = findBad prefixLen rest

satisfies :: Int -> [Int] -> Bool
satisfies prefixLen xs =
  hasSum (take prefixLen xs) (xs !! prefixLen)

hasSum :: [Int] -> Int -> Bool
hasSum xs n =
  or [n == x + y | x <- xs, y <- xs, x < y]

-- find contiguous series of ints that sum to n
findContiguous :: Int -> [Int] -> [Int]
findContiguous _ [] = []
findContiguous n xs@(_:rest)
  | map sum possibleSum == [n] = head possibleSum
  | otherwise = findContiguous n rest
  where
    possibleSum = take 1 $ dropWhile ((< n) . sum) [take i xs | i <- [2..length xs]]
