import Data.List (sort, group)
import Text.Printf (printf)

main :: IO ()
main = do
  -- Part One
  testdata1 <- (sort . map read . lines <$> readFile "day10.testdata1") :: IO [Int]
  testdata2 <- (sort . map read . lines <$> readFile "day10.testdata2") :: IO [Int]
  realdata <- (sort . map read . lines <$> readFile "day10.data") :: IO [Int]

  printf "Test data one: %d\n" (answer testdata1)
  printf "Test data two: %d\n" (answer testdata2)
  printf "Real data: %d\n" (answer realdata)

  printf "Part Two\n"
  printf "Test data one: %d\n" (part2 testdata1)
  printf "Test data two: %d\n" (part2 testdata2)
  printf "Real data: %d\n" (part2 realdata)

  -- Part Two

  where
    answer :: [Int] -> Int
    answer xs = count 3 (diffs xs) * count 1 (diffs xs)

    count :: Int -> [Int] -> Int
    count n = length . filter (== n)


diffs :: [Int] -> [Int]
diffs xs =
  [ y - x | (x, y) <- zip nums (tail nums) ]
  where
    nums = [0] ++ xs ++ [3 + maximum xs]


part2 :: [Int] -> Int
part2 =
  -- The product of the muber of combos we can do for all runs of adaptors that
  -- are directly adjacent to their neighbors (diff == 1)
  product . map (goofy . length) . filter (\xs -> take 1 xs == [1]) . group . diffs

-- The only adaptors that can be removed are the ones that are 1 away from a neighbor
-- Only when there are runs of more than 1 adjacent adaptors can you remove any.
-- The number of combinations is determined by the number of adjacent adaptors.
-- I don't know the formula but these are the values gotten by brute force.
-- Luckily, this is sufficient for the data given.
goofy :: Int -> Int
goofy 1 = 1
goofy 2 = 2
goofy 3 = 4
goofy 4 = 7
goofy 5 = 13
goofy _ = undefined
