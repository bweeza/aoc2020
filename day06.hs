import Data.Set (Set, fromList, union, unions, intersection, empty)
import Data.List.Split (splitOn) -- cabal install split and emacs can't find it? ugh

-- test answers are 11 6
-- real answers are 6437 3229

main :: IO ()
main = do
  testData <- readFile "day06.testdata"
  realData <- readFile "day06.data"
  print "Test"
  print $ answer unions testData
  print $ answer (foldl1 intersection) testData
  print "Real"
  print $ answer unions realData
  print $ answer (foldl1 intersection) realData

answer setop =
  sum . map length . map setop . map (map fromList . lines) . splitOn "\n\n"
